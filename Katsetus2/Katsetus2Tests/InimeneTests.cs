﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Katsetus2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Katsetus2.Tests
{
    [TestClass()]
    public class InimeneTests
    {
        [TestMethod()]
        public void ToProperTest()
        {
            Assert.AreEqual("Henn", Inimene.ToProper("henn"));
            Assert.AreEqual("Peeter", Inimene.ToProper("peeter"));
            Assert.AreEqual("Henn Sarv", Inimene.ToProper("henn sarv"));
            Assert.AreEqual("Peeter Suur", Inimene.ToProper("Peeter SUUR"));
            Assert.AreEqual("", Inimene.ToProper(""));
            Assert.AreEqual("Jaan-Magnus Sarv", Inimene.ToProper("jaan-magnus SARV"));
            Assert.AreEqual("Peetersuur", Inimene.ToProper("PeeterSUUR"));
        }

        [TestMethod()]
        public void InimeneConstructorTest()
        {
            Assert.AreEqual("Henn Sarv", (new Inimene { Nimi = "henn sarv" }).Nimi);
            Assert.AreEqual("Henn Sarv", (new Inimene("henn sarv")).Nimi);

        }
    }
}