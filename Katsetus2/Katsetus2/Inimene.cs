﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Katsetus2
{
    public class Inimene
    {
        string _Nimi;
        public string Nimi
        {
            get => _Nimi;
            set => _Nimi = ToProper(value);
        }

        public Inimene() { }
        public Inimene(string nimi) => Nimi = nimi;

        public static string ToProper(string nimi)
        {
            return nimi=="" ? "" : 
                string.Join(" ",
                nimi.Replace("-","- ")
                .Split(' ').Select(x => x.Substring(0, 1).ToUpper() + x.Substring(1).ToLower())
                ).Replace("- ","-");
        }
    }
}
