﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using TooJaVileHelp.Models;

namespace TooJaVileHelp.Controllers
{
    public class MyController : Controller
    {
        // neuid kahte funktsiooni pole nagu vaja -- nende asemele tegime kolmanda
        public bool AddDataFile(HttpPostedFileBase file, out int id )
        {
            id = 0;
            ToojavileEntities db = new ToojavileEntities();
            if (file != null && file.ContentLength > 0)
                using (BinaryReader br = new BinaryReader(file.InputStream))
                {
                    DataFile df = new DataFile
                    {
                        Content = br.ReadBytes(file.ContentLength),
                        FileName = file.FileName.Split('\\').Last().Split('/').Last(),
                        ContentType = file.ContentType,
                        Created = DateTime.Now
                    };
                    db.DataFiles.Add(df);
                    db.SaveChanges();
                    id = df.Id;
                    return true;
                }
            else return false;
        }
        public void RemoveDataFile(int? id)
        {
            if(id.HasValue)
            {
                ToojavileEntities db = new ToojavileEntities();
                db.DataFiles.Remove(db.DataFiles.Find(id.Value));
                db.SaveChanges();
            }
        }

        // sellel meetodil on kolm parameetrit
        // file - üles laetud (kui on) file
        // change - tegevus, mis oleks vaja teha üleslaetud pildi numbriga 
        // oldId - vana pildi number, mis tuleks seejärel kustutada
        public void ChangeDataFile(HttpPostedFileBase file, Action<int> change, int? oldId)
        {
            ToojavileEntities db = new ToojavileEntities();
            if (file != null && file.ContentLength > 0)
                using (BinaryReader br = new BinaryReader(file.InputStream))
                {
                    DataFile df = new DataFile
                    {
                        Content = br.ReadBytes(file.ContentLength),
                        FileName = file.FileName.Split('\\').Last().Split('/').Last(),
                        ContentType = file.ContentType,
                        Created = DateTime.Now
                    };
                    db.DataFiles.Add(df);
                    db.SaveChanges();
                    change(df.Id);
                    if (oldId.HasValue)
                    {
                        db.DataFiles.Remove(db.DataFiles.Find(oldId.Value));
                        db.SaveChanges();
                    }
                    
                }
        }
    }

    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}