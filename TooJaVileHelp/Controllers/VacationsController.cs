﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TooJaVileHelp.Models;

namespace TooJaVileHelp.Models
{
    public partial class Vacation
    {
        public static Dictionary<int, string> VacationTypes = new Dictionary<int, string>
        {
            {0, "unknown" },
            {1, "regular" },
            {2, "unpaid" },
            {3, "academic" },
            {4, "maternal" },
        };
        public static Dictionary<int, string> VacationStates = new Dictionary<int, string>
        {
            {0, "unknown" },
            {1, "submitted" },
            {2, "confirmed" },
            {3, "denied" },
            {4, "realized" },
        };

        public string VacationTypeName =>
            VacationTypes.ContainsKey(VacationType) ? VacationTypes[VacationType] : "";
        public string VacationStateName =>
            VacationStates.ContainsKey(VacationState) ? VacationStates[VacationState] : "";

    }

}

namespace TooJaVileHelp.Controllers
{
    public class VacationsController : Controller
    {

        private ToojavileEntities db = new ToojavileEntities();

        // GET: Vacations
        public ActionResult Index()
        {
            var vacations = db.Vacations.Include(v => v.Person);
            return View(vacations.ToList());
        }

        // GET: Vacations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vacation vacation = db.Vacations.Find(id);
            if (vacation == null)
            {
                return HttpNotFound();
            }
            return View(vacation);
        }

        // GET: Vacations/Create
        public ActionResult Create()
        {
            ViewBag.PersonId = new SelectList(db.People, "Id", "Email");
            ViewBag.VacationType = new SelectList(Vacation.VacationTypes, "Key", "Value");
            ViewBag.VacationState = new SelectList(Vacation.VacationStates, "Key", "Value");


            return View();
        }

        // POST: Vacations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,PersonId,StartDate,EndDate,VacationState,VacationType")] Vacation vacation)
        {
            if (ModelState.IsValid)
            {
                db.Vacations.Add(vacation);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PersonId = new SelectList(db.People, "Id", "Email", vacation.PersonId);
            ViewBag.VacationType = new SelectList(Vacation.VacationTypes, "Key", "Value");
            ViewBag.VacationState = new SelectList(Vacation.VacationStates, "Key", "Value");
            return View(vacation);
        }

        // GET: Vacations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vacation vacation = db.Vacations.Find(id);
            if (vacation == null)
            {
                return HttpNotFound();
            }
            ViewBag.VacationType = new SelectList(Vacation.VacationTypes, "Key", "Value", vacation.VacationType);
            ViewBag.VacationState = new SelectList(Vacation.VacationStates, "Key", "Value", vacation.VacationState);
            ViewBag.PersonId = new SelectList(db.People, "Id", "Email", vacation.PersonId);
            return View(vacation);
        }

        // POST: Vacations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PersonId,StartDate,EndDate,VacationState,VacationType")] Vacation vacation)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vacation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PersonId = new SelectList(db.People, "Id", "Email", vacation.PersonId);
            ViewBag.VacationType = new SelectList(Vacation.VacationTypes, "Key", "Value", vacation.VacationType);
            ViewBag.VacationState = new SelectList(Vacation.VacationStates, "Key", "Value", vacation.VacationState);
            return View(vacation);
        }

        // GET: Vacations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vacation vacation = db.Vacations.Find(id);
            if (vacation == null)
            {
                return HttpNotFound();
            }
            return View(vacation);
        }

        // POST: Vacations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Vacation vacation = db.Vacations.Find(id);
            db.Vacations.Remove(vacation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
