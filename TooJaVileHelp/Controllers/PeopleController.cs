﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TooJaVileHelp.Models;
using System.IO;

namespace TooJaVileHelp.Controllers
{
    public class PeopleController : MyController
    {
        private ToojavileEntities db = new ToojavileEntities();

        // GET: People
        public ActionResult Index()
        {
            var people = db.People.Include(p => p.Person2).Include(p => p.DataFile).Include(p => p.Department);
            return View(people.ToList());
        }

        // GET: People/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // GET: People/Create
        public ActionResult Create()
        {
            ViewBag.ParentId = new SelectList(db.People, "Id", "Email");
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType");
            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Name");
            return View();
        }

        // POST: People/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Email,FirstName,LastName,BirthDate,PictureId,DepartmentId,IsManager,IsAccountant,ParentId,IsStudent,VacationDays")] Person person)
        {
            if (ModelState.IsValid)
            {
                db.People.Add(person);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ParentId = new SelectList(db.People, "Id", "Email", person.ParentId);
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType", person.PictureId);
            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Name", person.DepartmentId);
            return View(person);
        }

        // GET: People/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            ViewBag.ParentId = new SelectList(db.People, "Id", "Email", person.ParentId);
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType", person.PictureId);
            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Name", person.DepartmentId);
            return View(person);
        }

        // POST: People/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Email,FirstName,LastName,BirthDate,PictureId,DepartmentId,IsManager,IsAccountant,ParentId,IsStudent,VacationDays")] Person person, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.Entry(person).State = EntityState.Modified;
                db.SaveChanges();
                //int? vanaPilt = person.PictureId;
                //if(AddDataFile(file, out int uusPilt))
                //{
                //    person.PictureId = uusPilt;
                //    db.SaveChanges();
                //    RemoveDataFile(vanaPilt);
                //}

                ChangeDataFile(
                    file, 
                    x => { person.PictureId = x; db.SaveChanges(); }, 
                    person.PictureId);

                //------------------- siia pildiga toimetamine
                //if (file != null && file.ContentLength > 0)
                //    using (BinaryReader br = new BinaryReader(file.InputStream))
                //    {
                //        DataFile df = new DataFile
                //        {
                //            FileName = file.FileName.Split('\\').Last(),
                //            ContentType = file.ContentType,
                //            Content = br.ReadBytes(file.ContentLength),
                //            Created = DateTime.Now,
                //        };
                //        db.DataFiles.Add(df);
                //        db.SaveChanges();
                //        // vana pildi kustutamiseks jätame meelde pildi numbri
                //        int? vanaPiltId = person.PictureId;
                //        person.PictureId = df.Id;
                //        db.SaveChanges();
                //        if (vanaPiltId.HasValue)
                //        {
                //            db.DataFiles.Remove(db.DataFiles.Find(vanaPiltId.Value));
                //            db.SaveChanges();
                //        }
                //    }


                // ------------------- siit lõpeb pildiga toimetamine

                return RedirectToAction("Index");
            }
            ViewBag.ParentId = new SelectList(db.People, "Id", "Email", person.ParentId);
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType", person.PictureId);
            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Name", person.DepartmentId);
            return View(person);
        }

        // GET: People/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Person person = db.People.Find(id);
            db.People.Remove(person);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
