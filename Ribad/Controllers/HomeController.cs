﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ribad.Models;

namespace Ribad.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Ribad()
        {
            List<Riba> ribad = new List<Riba>
            {
                new Riba {Nimi = "Henn", Algus = new DateTime(2019,3,7), Lopp = new DateTime(2019,7,3)},
                new Riba {Nimi = "Henn", Algus = new DateTime(2019,10,1), Lopp = new DateTime(2019,12,1)},
                new Riba {Nimi = "Ants", Algus = new DateTime(2019,4,7), Lopp = new DateTime(2019,8,3)},
                new Riba {Nimi = "Ants", Algus = new DateTime(2019,9,7), Lopp = new DateTime(2019,11,3)},
                new Riba {Nimi = "Peeter", Algus = new DateTime(2019,2,7), Lopp = new DateTime(2019,4,3)},
                new Riba {Nimi = "Peeter", Algus = new DateTime(2019,6,7), Lopp = new DateTime(2019,8,3)},
                new Riba {Nimi = "Peeter", Algus = new DateTime(2019,11,7), Lopp = new DateTime(2019,12,31)},
            };
            return View(ribad);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}