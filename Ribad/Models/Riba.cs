﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Ribad.Models
{
    public class Riba
    {
        public string Nimi;
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0}")]
        public DateTime Algus { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0}")]
        public DateTime Lopp { get; set; }
    }
}