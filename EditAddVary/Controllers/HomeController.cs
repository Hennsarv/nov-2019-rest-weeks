﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EditAddVary.Controllers
{

    public class UniversalBag : DynamicObject 
    {
        dynamic bag;
        public UniversalBag() => bag = new Dictionary<string, dynamic>();
        public UniversalBag(dynamic bag) => this.bag = bag;

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            switch (bag)
            {
                case IDictionary<string, object> d:
                    result = d.ContainsKey(binder.Name) ? d[binder.Name] : null;
                    return true;
                default:
                    result = bag[binder.Name];
                    return true;
            }

        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            bag[binder.Name] = value;
            return true;
        }

        

    }

    public class MyController : Controller 
    {
        protected dynamic TempBag;
        dynamic sessionBag;
        protected dynamic SessionBag => sessionBag ?? (sessionBag = new UniversalBag(System.Web.HttpContext.Current.Session));
        dynamic paramBag;
        protected dynamic RequestBag => paramBag ?? (paramBag = new UniversalBag(Request));
        dynamic appBag;
        protected dynamic AppBag => appBag ?? (appBag = new UniversalBag(System.Web.HttpContext.Current.Application));
        HttpCookie bagCookie;
        protected HttpCookie BagCookie => 
            bagCookie 
                ?? (bagCookie = Request.Cookies["bagtest"])
                ?? (bagCookie = new HttpCookie("bagtest"))
            ;
        dynamic cookieBag;
        protected dynamic CookieBag => cookieBag ?? (cookieBag = new UniversalBag(BagCookie)); 
        public MyController()
        {
            TempBag = new UniversalBag(TempData);
            
        }

        protected void SetCookie(string name, object value, int hours = 1)
        {
            if (value == null) return;
                BagCookie.Values[name] = value.ToString();
                BagCookie.Expires = DateTime.Now.AddHours(hours);
                Response.Cookies.Add(BagCookie);
        }

        
    }

    [Authorize]
    public class HomeController : Controller
    {
        
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}