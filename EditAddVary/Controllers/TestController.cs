﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EditAddVary.Controllers
{
    public class TestController : MyController
    {
        // GET: Test
        public ActionResult Index(string nimi, int id = 0)
        {
            ViewBag.Result = $"id={id} nimi={nimi} muu={RequestBag.muu} Muu={RequestBag.Muu} nupp={RequestBag.Nupp}";
            ViewBag.Params = Request.Params;
            ViewBag.Query  = Request.QueryString;

            int appStart = ++AppBag.Start;      // Application["Start"]
            int sessStart = ++SessionBag.Start; // Session["Start"]
            
            ViewBag.Started = $"App {appStart} {AppBag.Started.ToShortTimeString()} " +
                              $"Sess {sessStart} {SessionBag.Started.ToShortTimeString()}";

            ViewBag.Test = RequestBag.Test; // Params["Test"]
            
            SetCookie("Kook", RequestBag.Kook);   // Params["Kook"]
                                                // HttpCookie h = new HttpCookie("BagTest")
                                                // h["Kook"] = Params["Kook"]
                                                // h.Expires = DateTime.Now.AddHours(hours);
                                                // Response.Cookies.add(h)
            ViewBag.Kook = CookieBag.Kook;      
                            // HttpCookie h = new HttpCookie["BagTest"]
                            // või Request.Cookies["BagTest"]
                            // ViewBag.Kook = h["Kook"]
                            // jne

            return View();
        }

        public ActionResult Test()
        {
            ViewBag.Tekst = "Seda juttu ma näitan vius";


            return View();
        }

    }
}