﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EditAddVary.Startup))]
namespace EditAddVary
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
