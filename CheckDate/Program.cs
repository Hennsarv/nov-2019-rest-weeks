﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckDate
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Booking> bookingud = new List<Booking>
            {
                new Booking {Nimi = "Henn", Algus = new DateTime(2019,11,22,10,00,00),Tunde=3},
                new Booking {Nimi = "Ants", Algus = new DateTime(2019,11,23,10,00,00),Tunde=3},
                new Booking {Nimi = "Henn", Algus = new DateTime(2019,11,27,12,00,00),Tunde=3},
                new Booking {Nimi = "Peeter", Algus = new DateTime(2019,11,26,10,00,00),Tunde=3},
                new Booking {Nimi = "Kaarel", Algus = new DateTime(2019,11,22,15,00,00),Tunde=3},
                new Booking {Nimi = "Henn", Algus = new DateTime(2019,11,22,18,00,00),Tunde=3},
            };

            var tunnid = bookingud.Where(x => x.Algus.Date == DateTime.Today) // valin tänase päeva bookingud
                .Select(x => new { x.Algus.Hour, x.Tunde }) // leian bookingute algusajad ja tunnid
                .Select(x => Enumerable.Range(x.Hour, x.Tunde)) // leian boogitud tunnid
                .SelectMany(x => x).ToArray(); // teen nad jadaks ja siis arrayks

            Console.WriteLine(string.Join(",", tunnid));

            var vabad = Enumerable.Range(0, 24)
                .Select(x => new { Tund = x, Vaba = tunnid.Contains(x) });

            Console.WriteLine(string.Join(",", vabad));

        }
    }

    class Booking
    {
        public string Nimi;
        public DateTime Algus;
        public int Tunde;

    }
}
