Datepickeri lisamiseks - mis ma tegin

1. Employee mudelis lisasin HireDate ja BirthDate vljadele
	data annotatsioonid (nurksulgudes osad seal ees)

2. Employee Edit view peale panin nendele kahele vljale lisaks datepicker klassi

3. _Layout vaatele lisasin paar scripti
	datepickeri initsialiseerimiseks read 48-57
	eestikeelse toe llisamiseks read 58-100

4. Valisin nuget managerist jquery.ui miskine mooduli
	install-package jquery.ui 

5. Lisasin klassi

	DateTimeModelBinder // proovi, kas saab ka ilma

6. Lisasin kaks rida _Layout viewsse

	Rida 8 ja rida rida 42

7. Lisasin kuupevavljade editorFor asjadele klassi datepicker
   @Html.EditorFor(model => model.BirthDate, 
   new { htmlAttributes = new { @class = "form-control datepicker" } })

	PEALE NEID ASJU HAKKAS MUL DATEPICKER ilusti tle
