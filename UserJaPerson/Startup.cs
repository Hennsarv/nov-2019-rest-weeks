﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(UserJaPerson.Startup))]
namespace UserJaPerson
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
