﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UserJaPerson.Models;
using System.IO;
using System.Data.SqlClient;

namespace UserJaPerson.Controllers
{
    public class MyController : Controller
    {
        // TODO: muuda ära dataContext
        // datacontexti access võix olla protected, 
        // siis saavad MyController tüüpi kontrollerid seda sama datacontexti kasutada
        NorthwindEntities ldb = null;
        protected NorthwindEntities db => ldb ?? (ldb = new NorthwindEntities(
            Session["db"]?.ToString() ?? "Northwind"
            ));

        

        // võtsin selle rea ära, et saaks erinevates andmebaasides kolada
        // panin tagasi, muutsin Param = "*" ja lisasin teise parameetri
        // muidu tulid teise andmebaasi sama numbriga failid cacheist
        [OutputCache(Duration =3600, VaryByParam = "*", Location = System.Web.UI.OutputCacheLocation.Any)]
        public ActionResult Content(int? id,string database)
        {
            
            DataFile df = db.DataFiles.Find(id);
            if (df == null) return HttpNotFound();
            return File(df.Content, df.ContentType);
        }
        
        protected Person CurrentPerson = null; // siis on hea kohe see PersonObject omale taskusse panna
        protected void CheckPerson()
        {
           
            if (Request.IsAuthenticated)        // kontrollida vaja vaid siis
                if ((CurrentPerson?.EMail ?? "") != User.Identity.Name)    // kui juba ei ole meeles või on vale
                    {
                        CurrentPerson = db.People.Where(x => x.EMail == User.Identity.Name).SingleOrDefault();
                        if (CurrentPerson == null)
                        {
                            using (ApplicationDbContext dba = new ApplicationDbContext())
                            {
                                ApplicationUser u = dba.Users.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
                                db.People.Add(CurrentPerson = new Person
                                {
                                    EMail = User.Identity.Name,
                                    FirstName = u.FirstName,
                                    LastName = u.LastName,
                                    BirthDate = u.BirthDate
                                });
                                db.SaveChanges();
                            }
                        }
                    }
                else CurrentPerson = null;
        }

        // file - kontrollerisse post-requestiga üles laetud fail
        // oldId - vana faili number, mis baasist kustutada (null - mitte kustutada
        // change - tegevus, mis lisamise ja kustutamise vahel vaja teha
        //  x => {person.PictureId = x; db.SaveChanges();}
        protected void ChangeDataFile(HttpPostedFileBase file, Action<int> change, int? oldId)
        {
            if (file != null && file.ContentLength > 0)
                using (BinaryReader br = new BinaryReader(file.InputStream))
                {
                    DataFile df = new DataFile
                    {
                        Content = br.ReadBytes(file.ContentLength),
                        FileName = file.FileName.Split('\\').Last().Split('/').Last(),
                        ContentType = file.ContentType,
                        Created = DateTime.Now
                    };
                    db.DataFiles.Add(df);
                    db.SaveChanges();
                    change(df.Id);
                    if (oldId.HasValue)
                    {
                        db.DataFiles.Remove(db.DataFiles.Find(oldId.Value));
                        db.SaveChanges();
                    }

                }
        }

    }

}