﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UserJaPerson.Models;
using System.IO;

namespace UserJaPerson.Models
{
    public class Studio
    {
        static int nr = 0;
        public int Id { get; }
        public string Nimi { get; set; }

        static Dictionary<int, Studio> _Stuudiod = new Dictionary<int, Studio>();
        public static IEnumerable<Studio> Stuudiod => _Stuudiod.Values;
        static Studio() => new List<Studio>
        {
            new Studio {Nimi = "Pärnu"},
            new Studio {Nimi = "Tartu"},
            new Studio {Nimi = "Viljandi"},

        };
        public Studio () { Id = ++nr; _Stuudiod.Add(Id, this); }
        public static Studio Find(int id) => _Stuudiod.ContainsKey(id) ? _Stuudiod[id] : null;
        public IEnumerable<Booking> Bookings => Booking.Bookings.Where(x => x.StudioId == Id);
    }
    public class Booking
    {
        static int nr = 0;
        public int id { get; } = ++nr;
        public int StudioId { get; set; }
        public DateTime Kuupäev { get; set; }
        public int Kell { get; set; }
        public int Tunde { get; set; }
        public string Nimi { get; set; } = "booked";

        public Studio Studio => Studio.Find(StudioId);

        static List<Booking> _Bookings;
        public static IEnumerable<Booking> Bookings => _Bookings.AsEnumerable();

        public static void Add(Booking booking) => _Bookings.Add(booking);

        static Booking()
        {
            Random r = new Random();
            _Bookings = Enumerable.Range(1, 100)
                .Select(x => new Booking
                {
                    StudioId = r.Next(1, Studio.Stuudiod.Count()+1),
                    Kuupäev = DateTime.Today.AddDays(r.Next(0, 14)),
                    Kell = r.Next(10,21),
                    Tunde = r.Next(1,4)
                }
                )
                .ToList()
                ;
        }

    }


}

namespace UserJaPerson.Controllers
{
   
    public class HomeController : MyController
    {
       

         public ActionResult Index()
        {
            CheckPerson(); // selle panen igale poole, kus vaja personi kontrollida
            ViewBag.CurrentPerson = CurrentPerson;
            
            return View();
        }

        public ActionResult About(int? StudioId, DateTime? Day)
        {
            ViewBag.Message = "Your application description page.";
            ViewBag.StudioId = new SelectList(Studio.Stuudiod, "Id", "Nimi", StudioId);
            ViewBag.Day = Day ?? (Day = DateTime.Today);
            ViewBag.Bookings = Booking.Bookings
                .Where(x => x.Kuupäev >= Day.Value && x.Kuupäev <= Day?.AddDays(7) && x.StudioId == StudioId);

            return View();
        }

        public ActionResult Contact(string id = "")
        {
            ViewBag.Message = "Your contact page.";

            ViewBag.Stuudiod = Studio.Stuudiod;

            Studio s = Studio.Stuudiod.Where(x => x.Nimi == id).SingleOrDefault();
            ViewBag.Bookings = s?.Bookings;

            return View();
        }

        public ActionResult Book(DateTime Day, int Time, int StudioId)
        {
            Booking.Add(new Booking { Kuupäev = Day, Kell = Time, StudioId = StudioId, Tunde = 1, Nimi ="Added" });
            return RedirectToAction("About", new { Day, StudioId });
        }
    }
}