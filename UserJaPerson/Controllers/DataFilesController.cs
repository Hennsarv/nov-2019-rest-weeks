﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UserJaPerson.Models;

namespace UserJaPerson.Models
{
    public class DataFileInfo // selle klassi tegin, et ei peaks kõiki faile andmebaasist lugema
    // selles klassis on KÕIK andmebaasi väljad peale faili sisu
    {
        public int Id { get; set; }
        public string ContentType { get; set; }
        public string FileName { get; set; }
        public System.DateTime Created { get; set; }

        // sellise asja lisasin, et saaks hõlpsasti seda tüüpi objekte teha
        // oleks võinud selle asemel ka konstruktorit kasutada

        //public static explicit operator DataFileInfo ((int id, string contentType, string fileName, System.DateTime created) info)
        //    => new DataFileInfo { Id = info.id, ContentType = info.contentType, Created = info.created, FileName = info.fileName};

        // sama asi siis contruktorina oleks
        //public DataFileInfo() { } // default contruktor, et ei peaks ogu aeg
        //public DataFileInfo((int id, string contentType, string fileName, System.DateTime created) info)
        //    => (Id, ContentType, FileName, Created) = info;

        // NB! tuplit kasutan siin parameetrina, et saaks lühemalt kirjutada
    }
}

namespace UserJaPerson.Controllers
{
    public class DataFilesController : MyController
    {
 
        // GET: DataFiles
        public ActionResult Index()
        {
            string database = (Request.Params["db"] ?? Session["db"] ?? null)?.ToString()??"Northwind";
            Session["db"] = database;
            

            Dictionary<string, string> databases = new Dictionary<string, string>
            {
                {"Henn", "Northwind" },
                {"Huvikool", "Tagarida" },
                {"Eelarveldajad", "Budget" },
                {"Puhkajad", "TooJaVile" },
                {"Fotostuudiod", "FBR" },
                {"Koerajalutajad", "Patrull" },
                {"Reisimehed", "KolmTenorit" },
                {"Aednikud", "Aednikud" }

            };
            ViewBag.db = new SelectList(databases, "Value", "Key", database);

            var q = db.DataFiles.Select(x => new { x.Id, x.Created, x.ContentType, x.FileName })
                .ToList()
                //.Select(x => (DataFileInfo)(x.Id, x.FileName, x.ContentType, x.Created))
                //.Select(x => new DataFileInfo((x.Id, x.FileName, x.ContentType, x.Created)))
                .Select(x => new DataFileInfo { Id = x.Id, FileName = x.FileName, ContentType = x.ContentType, Created = x.Created })
                .ToList();
            return View(q);
        }

        // GET: DataFiles/Details/5
        public ActionResult Details(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            DataFile dataFile = db.DataFiles.Find(id);
            if (dataFile == null)
            {
                return HttpNotFound();
            }
            return View(dataFile);
        }

        // GET: DataFiles/Create
 
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
