﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UserJaPerson.Models;

namespace UserJaPerson.Models
{
    partial class Person
    {
        public string FullName => $"{FirstName} {LastName}";
    }
}

namespace UserJaPerson.Controllers
{
    public class PeopleController : MyController
    {

        // need neli funktsiooni ma tegin lihtsalt et demoda kuupäevaarvutuisi
        // esimesed kolm arvutavad 
        public int DaysInCurrentMonth()
        => (DateTime.Today - DateTime.Today.AddMonths(1)).Days;
        public int DaysInThisMonth(DateTime dt) => (dt - dt.AddMonths(1)).Days;
        public int DaysInThisMonth(int year, int month)
            //=> (new DateTime(year, month, 1) - new DateTime(year, month, 1).AddMonths(1)).Days;
            => DaysInThisMonth(new DateTime(year, month, 1));
        public int DaysInFeb(int year)
            => year % 4 != 0 ? 28 :
                year % 400 == 0 ? 29 :
                year % 100 == 0 ? 28 : 29;


        // private NorthwindEntities db = new NorthwindEntities();

        // GET: People
        public ActionResult Index()
        {
            return View(db.People.ToList());
        }

        // GET: People/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // GET: People/Create
        public ActionResult Create()
        {
            return View(new Person { EMail = "" });
        }

        // POST: People/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FirstName,LastName,EMail,BirthDate,Nationality")] Person person
            , HttpPostedFileBase file           // lisame ka create meetodile pildi 
            )
        {
            // lisame siia mõne kontrolli, mida automaatselt ei saa teha
            // 1. meiliaadress peab olema unikaalne
            // 2. nimi peab olema unikaalne
            if(person.EMail != "" && db.People.Where(x => x.EMail == person.EMail).Count() > 0)
                // kuna kontoll tehakse andmebasi päringuga, siis ei pea muretsema suur-väiketäht eest
            {
                ModelState.AddModelError("EMail", "selline meiliaadress juba on");
            }

            if(db.People.Where(x => x.FirstName + " " + x.LastName == person.FullName).Count() > 0)
            {
                ModelState.AddModelError("FirstName", "nimi kordub - vali teine nimi");
            }

            if (ModelState.IsValid)
            {
                db.People.Add(person);
                db.SaveChanges();
                // siia vaja lisada vaid 1 rida
                ChangeDataFile(file, x => { person.PictureId = x; db.SaveChanges(); }, null);
                return RedirectToAction("Index");
            }

            return View(person);
        }

        // GET: People/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FirstName,LastName,EMail,BirthDate,Nationality")] Person person, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.Entry(person).State = EntityState.Modified;
                db.SaveChanges();
                ChangeDataFile(
                                file,
                                x => { person.PictureId = x; db.SaveChanges(); },
                                person.PictureId);
                return RedirectToAction("Index");
            }
            return View(person);
        }

        // GET: People/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Person person = db.People.Find(id);
            db.People.Remove(person);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
